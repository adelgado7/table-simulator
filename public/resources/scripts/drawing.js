var app = angular.module("drawing", ["firebase"]);
app.factory("drawSync", function ($firebaseArray, $firebaseObject) {
    this.create = function (stage, lines, ref, clientId) {
        var drawSync = this;
        var stage = stage;
        var lineShapes = {};
        this.update = function () {
            stage.canvas.width = stage.canvas.clientWidth;
            stage.canvas.height = stage.canvas.clientHeight;
            stage.update();
        }
        this.addPoint = function (point) {
            if (point.prev) {
                var color = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
                lineShapes[point.lineId].graphics
                    .beginStroke(color)
                    .setStrokeStyle(10, "round")
                    .moveTo(point.prev.x, point.prev.y)
                    .lineTo(point.x, point.y);
            }
        }

        var loadingComplete = false;
        var watchLine = function (id) {
            var pointArr = $firebaseArray(ref.child(id));
            var obj = $firebaseObject(ref.child(id))
            obj.$loaded(function () {
                if (obj.drawing) {
                    pointArr.$watch(function (event) {
                        if (obj.drawing) {
                            if (event.event == "child_added") {
                                var index = pointArr.$indexFor(event.key);
                                if (index > 1) {
                                    var point = pointArr[index];
                                    var prev = pointArr[index - 1];
                                    drawSync.addPoint({
                                        "x": point.x,
                                        "y": point.y,
                                        "prev": prev,
                                        "lineId": id
                                    });
                                    if (loadingComplete) {
                                        stage.update();
                                    }
                                }
                            }
                        } else {
                            obj.$destroy();
                            pointArr.$destroy();
                        }
                    });
                }
            })
        }

        var createShape = function (id) {
            var shape = new createjs.Shape();
            lineShapes[id] = shape;
            stage.addChild(shape);
        }

        lines.$loaded().then(function (lineArr) {
            lineArr.forEach(function (line) {
                createShape(line.$id);
                if (line.drawing) {
                    watchLine(line.$id);
                } else {
                    var prev;
                    for (point in line) {
                        if (line[point]) {
                            point = line[point];
                            drawSync.addPoint({
                                "x": point.x,
                                "y": point.y,
                                "prev": prev,
                                "lineId": line.$id
                            });
                            prev = point;
                        }
                    }
                }
            });
            lines.$watch(function (event) {
                if (event.event == "child_added") {
                    if (lines.$getRecord(event.key).drawing && !lineShapes[event.key]) {
                        createShape(event.key);
                        watchLine(event.key);
                    }
                } else if (event.event == "child_removed" && lineShapes[event.key]) {
                    stage.removeChild(lineShapes[event.key]);
                    stage.update();
                }
            });
            drawSync.update();
            loadingComplete = true;
        });
        return this;
    };
    return this;
});
app.directive("drawCanvas", ["drawSync", function ($window) {
    return {
        restrict: "E",
        templateUrl: "resources/html/draw-canvas.html",
        controller: function ($firebaseArray, $firebaseObject, drawSync, $window) {
            var ctrl = this;
            var ref = new Firebase("https://scorching-torch-8142.firebaseio.com/").child("line_drawings");
            var lines = $firebaseArray(ref);
            var stage = new createjs.Stage("drawing");
            var sync = drawSync.create(stage, lines, ref);

            var line = {};
            var lineObj = {};
            var drawing = false;
            this.startDrawing = function (event) {
                if (drawing) {
                    ctrl.endDrawing();
                }
                line.drawing = true;
                lines.$add(line).then(function (lineRef) {
                    line = $firebaseArray(ref.child(lineRef.key()));
                    lineObj = $firebaseObject(ref.child(lineRef.key()));
                    lineObj.drawing = true;
                    lineObj.$save();
                    ctrl.addPoint(event);
                    drawing = true;
                });;
            }

            this.addPoint = function (event) {
                if (drawing) {
                    line.$add({
                        "x": event.offsetX,
                        "y": event.offsetY
                    });
                }
            }

            this.endDrawing = function (event) {
                drawing = false;
                lineObj.drawing = false;
                lineObj.$save().then(function () {
                    line.$destroy();
                    lineObj.$destroy();
                });
            }

            this.clear = function () {
                if (drawing) {
                    endDrawing();
                }
                $firebaseObject(ref).$remove();
            }

            angular.element($window).bind('resize', function () {
                sync.update();
            });
        },
        controllerAs: "drawing"
    };
}]);
