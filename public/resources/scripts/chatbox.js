var app = angular.module("chat", ["firebase"]);
app.directive("chatBox", function () {
    return {
        restrict: "E",
        templateUrl: "resources/html/chat-box.html",
        controller: function ($scope, $firebaseArray, $location, $anchorScroll) {
            var ref = new Firebase("https://scorching-torch-8142.firebaseio.com/").child("chat_messages");
            this.messages = $firebaseArray(ref);
            var chat = this;
            // create a synchronized array
            this.newMessageText = "";
            this.name = "";
            $location.hash("chat-bottom");
            // add new items to the array
            // the message is automatically added to our Firebase database!
            this.addMessage = function () {
                chat.messages.$add({
                    "name": chat.name,
                    "tstamp": new Date().getTime(),
                    "text": chat.newMessageText
                });
                chat.newMessageText = "";
            };
            this.messages.$watch(function (event) {
                if (event.event == "child_added") {
                    $scope.scrollBottom();
                }
            });
            $scope.scrollBottom = function () {
                $anchorScroll();
            }
        },
        controllerAs: "chat",
        link: function (scope) {
            scope.scrollBottom();
        }
    };
});