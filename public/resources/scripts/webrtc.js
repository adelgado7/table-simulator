var app = angular.module("webrtc", ["firebase"]);
app.directive("webRtc", function ($firebaseArray) {
    return {
        restrict: "E",
        templatUrl: "resources/html/web-rtc.html",
        link: function (element) {
            // Handle prefixed versions
            navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
            var ref = new Firebase("https://scorching-torch-8142.firebaseio.com/").child("voice_participants");
            participants = $firebaseArray(ref);

            // State
            var me = {};
            var myStream;
            var peers = {};
            this.message = "";

            init();

            // Start everything up
            function init() {
                if (!navigator.getUserMedia) return unsupported();

                getLocalAudioStream(function (err, stream) {
                    if (err || !stream) return;

                    connectToPeerJS(function (err) {
                        if (err) return;

                        registerIdWithServer(me.id);
                        if (participants.length) callPeers();
                        else displayShareMessage();
                    });
                });
            }

            // Connect to PeerJS and get an ID
            function connectToPeerJS(cb) {
                display('Connecting to PeerJS...');
                me = new Peer({
                    key: 'w0eezxyrxd6de7b9'
                });

                me.on('call', handleIncomingCall);

                me.on('open', function () {
                    display('Connected.');
                    display('ID: ' + me.id);
                    cb && cb(null, me);
                });

                me.on('error', function (err) {
                    display(err);
                    cb && cb(err);
                });
            }

            // Add our ID to the list of PeerJS IDs for this call
            function registerIdWithServer() {
                //Use AngularFire to update call list
                participants.$add({
                    id: me.id
                });
                //                display('Registering ID with server...');
                //                $.post('/' + call.id + '/addpeer/' + me.id);
            }

            function unregisterIdWithServer() {
                //Use AngularFire to remove from call list
                participants.$remove({
                    id: me.id
                });
                //                $.post('/' + call.id + '/removepeer/' + me.id);
            }

            // Call each of the peer IDs using PeerJS
            function callPeers() {
                participants.forEach(callPeer);
            }

            function callPeer(peer) {
                display('Calling ' + peer.id + '...');
                if (peer.id != me.id) {
                    var peer = getPeer(peer.id);
                    peer.outgoing = me.call(peer.id, myStream);

                    peer.outgoing.on('error', function (err) {
                        display(err);
                    });

                    peer.outgoing.on('stream', function (stream) {
                        display('Connected to ' + peer.id + '.');
                        addIncomingStream(peer, stream);
                    });
                }
            }

            // When someone initiates a call via PeerJS
            function handleIncomingCall(incoming) {
                display('Answering incoming call from ' + incoming.peer);
                var peer = getPeer(incoming.peer);
                peer.incoming = incoming;
                incoming.answer(myStream);
                peer.incoming.on('stream', function (stream) {
                    addIncomingStream(peer, stream);
                });
            }

            // Add the new audio stream. Either from an incoming call, or
            // from the response to one of our outgoing calls
            function addIncomingStream(peer, stream) {
                display('Adding incoming stream from ' + peer.id);
                peer.incomingStream = stream;
                playStream(stream);
            }

            // Create an <audio> element to play the audio stream
            function playStream(stream) {
                //var audio = angular.element('<audio autoplay />');
                var audio = new Audio((URL || webkitURL || mozURL).createObjectURL(stream));
                audio.play();
            }

            // Get access to the microphone
            function getLocalAudioStream(cb) {
                display('Trying to access your microphone. Please click "Allow".');

                navigator.getUserMedia({
                        video: false,
                        audio: true
                    },

                    function success(audioStream) {
                        display('Microphone is open.');
                        myStream = audioStream;
                        if (cb) cb(null, myStream);
                    },

                    function error(err) {
                        display('Couldn\'t connect to microphone. Reload the page to try again.');
                        if (cb) cb(err);
                    }
                );
            }

            ////////////////////////////////////
            // Helper functions
            function getPeer(peerId) {
                return peers[peerId] || (peers[peerId] = {
                    id: peerId
                });
            }

            function displayShareMessage() {
                //                display('Give someone this URL to chat.');
                //                display('<input type="text" value="' + location.href + '" readonly>');
                //
                //                $('#display input').click(function () {
                //                    this.select();
                //                });
            }

            function unsupported() {
                display("Your browser doesn't support getUserMedia.");
            }

            function display(message) {
                this.message = message;
            }
        },
        controllerAs: "webrtc"
    };
});
